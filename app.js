var http = require("http"),
  socketio = require("socket.io"),
  fs = require("fs");
 
//var app = require('express').createServer();
var app = http.createServer(function(req, resp){
  // This callback runs when a new connection is made to our HTTP server.
 
  fs.readFile("index.html", function(err, data){
    // This callback runs when the client.html file has been read from the filesystem.
 
    if(err) return resp.writeHead(500);
    resp.writeHead(200);
    resp.end(data);
  });
});
app.listen(3456);
var io = require('socket.io').listen(app);

// usernames which are currently connected to the chat key = user and value = room
var usernames = {};

// rooms which are currently available in chat
// make this a map with room name as key and users as values!!!!!!!!!!!!
var rooms = ['lobby'];
var AllRooms = {'lobby': ''};


//dictionary where key = room and value = user who is banned from entering that room
var banned = {};

io.sockets.on('connection', function (socket) {

  // when the client emits 'adduser', this listens and executes
  socket.on('adduser', function(username){
    // store the username in the socket session for this client
    socket.username = username;
    // store the room name in the socket session for this client
    socket.room = 'lobby';
    // add the client's username to the global list
    usernames[username] = socket.room;

    // send client to lobby
    socket.join('lobby');
    // echo to client they've connected
    socket.emit('updatechat', 'SERVER', 'you have connected to lobby');
    //console.log("added user");
    // echo to lobby that a person has connected to their room
    socket.broadcast.to('lobby').emit('updatechat', 'SERVER', username + ' has connected to this room');
    socket.emit('updaterooms', rooms, 'lobby', usernames);
    socket.broadcast.emit('updateOtherUsers', rooms, usernames);
  });

  socket.on('passwordLookup', function(room){
    var pw = AllRooms[room];
    //console.log(room + " " + pw);
    socket.emit('switchRoom', room, pw);
  });

  // when the client emits 'sendchat', this listens and executes
  socket.on('sendchat', function (data) {
    // we tell the client to execute 'updatechat' with 2 parameters
    io.sockets.in(socket.room).emit('updatechat', socket.username, data);
  });

  socket.on('switchRoom', function(newroom){

    // leave the current room (stored in session)
    socket.leave(socket.room);

    // sent message to OLD room
    socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username+' has left this room');

    // join new room, received as function parameter
    socket.join(newroom);
    socket.emit('updatechat', 'SERVER', 'you have connected to '+ newroom);

    //update usernames array to include new room they've switched to
    usernames[socket.username] = newroom;
    
    // update socket session room title
    socket.room = newroom;
    socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username+' has joined this room');
    socket.emit('updaterooms', rooms, newroom, usernames);
    socket.broadcast.emit('updateOtherUsers', rooms, usernames);

  });

  // when the client emits 'addroom' this listens and adds a new room
  socket.on('addRoom', function(newroom, pw) {

    //add it to the list of rooms
    rooms.push(newroom);
    AllRooms[newroom] = pw;

    socket.emit('updateAddRoom', rooms, newroom);
    socket.broadcast.emit('updateAddRoom', rooms, newroom);
    
  });

  socket.on('kickUser', function(user) {
      if(!usernames.hasOwnProperty(user)) {
        socket.emit('invalidUser');
      } else {
        //if the person is in the room kick them out to lobby
        if (usernames[user] == socket.room) {
          //call kickOut function that will switch the other user to lobby
          socket.broadcast.emit('kickOut', user);
          usernames[user] = 'lobby';
          // Send over information of people who are in the room to the client. 
          socket.emit('updaterooms', rooms, 'lobby', usernames);
          socket.broadcast.emit('updateOtherUsers', rooms, usernames);
        }
      }
  });

  socket.on('banUser', function(bannedroom, user) {
    if(!usernames.hasOwnProperty(user)) {
        //console.log("invalid user to kick out!");
        socket.emit('invalidUser');
    } else {
      if (usernames[user] == socket.room) {
        //call kickOut function that will switch the other user to lobby
        socket.broadcast.emit('kickOut', user);
        usernames[user] = 'lobby';
        // Send over information of people who are in the room to the client. 
        socket.emit('updaterooms', rooms, 'lobby', usernames);
        socket.broadcast.emit('updateOtherUsers', rooms, usernames);
        
        socket.broadcast.emit('ban', bannedroom, user);
      }
    }
  });

  socket.on('privateMsg', function(user, msg, sender) {
    if(!usernames.hasOwnProperty(user)) {
        //console.log("invalid user to kick out!");
        socket.emit('invalidUser');
      } else {
        if (usernames[user] == socket.room) {
          //call kickOut function that will switch the other user to lobby
          socket.broadcast.emit('privateMsg', user, msg, sender);
        }
      }
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function(){
    // remove the username from global usernames list
    delete usernames[socket.username];
    // update list of users in chat, client-side
    io.sockets.emit('updateusers', usernames);
    // echo globally that this client has left
    socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
    socket.leave(socket.room);

    // Send over information of people who are in the room to the client. 
    //socket.emit('updaterooms', rooms, 'null', );
  });
});